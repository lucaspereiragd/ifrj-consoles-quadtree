class QuadTree{
	constructor(boundary, capacity){
		this.boundary = boundary;
		this.capacity = capacity;
		this.points = [];
		this.divided = false;
	}
	
	build(){
		var x = this.boundary.x;
		var y = this.boundary.y;
		var w = this.boundary.w;
		var h = this.boundary.h;
		
		var ne = new Rectangle(x + w/2, y, w/2, h/2);
		this.northeast = new QuadTree(ne, this.capacity);
		var nw = new Rectangle(x, y, w/2, h/2);
		this.northwest = new QuadTree(nw, this.capacity);
		var se = new Rectangle(x + w/2, y + h / 2, w/2, h/2);
		this.southeast = new QuadTree(se, this.capacity);
		var sw = new Rectangle(x, y + h / 2, w/2, h/2);
		this.southwest = new QuadTree(sw, this.capacity);
		this.divided = true;
	}
	
	adicionar(point){
		
		if(!this.boundary.contains(point))
			return false;
		
		if(this.points.length < this.capacity){
			this.points.push(point);
			return true;
		}else {
			if (!this.divided)
				this.build();
				
			if (this.northeast.adicionar(point))
				return true;
			else if (this.northwest.adicionar(point))
				return true;
			else if (this.southeast.adicionar(point))
				return true;
			else if (this.southwest.adicionar(point))
				return true;
		}
	}
	
	consultar(range, found){
		if(!found)
			found = [];
		if(!this.boundary.intersects(range)){
			return;
		}else{
			for(var i = 0; i < this.points.length; i++){
				if(range.contains(this.points[i]))
					found.push(this.points[i]);
			}
			
			if(this.divided){
				this.northeast.consultar(range, found);
				this.northwest.consultar(range, found);
				this.southeast.consultar(range, found);
				this.southwest.consultar(range, found);
			}
		}
		return found;
	}
	
	show(ctx){
		ctx.strokeStyle = "#FFFFFF";
		ctx.lineWidth = 1;
		ctx.strokeRect(this.boundary.x, this.boundary.y, this.boundary.w, this.boundary.h);
		if(this.divided){
			this.northeast.show(ctx);
			this.northwest.show(ctx);
			this.southeast.show(ctx);
			this.southwest.show(ctx);
		}
		
		for(var i = 0; i < this.points.length; i++){
			ctx.fillStyle = "#FFFFFF";
			ctx.fillRect(this.points[i].x, this.points[i].y, 2, 2);
		}
			
	}
}
