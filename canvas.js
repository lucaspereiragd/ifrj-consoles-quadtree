function exec_seq(samples, points) {
	var canvas = document.getElementById('canvas');
	canvas.width = 400;
	canvas.height = 400;

	var ctx = canvas.getContext('2d');

	var boundary = new Rectangle(0, 0, 400, 400);
	var qtree = new QuadTree(boundary, 1);

	for (var i = 0; i < samples; i++) {
		//adiciona um ponto no quadtree qtree
		qtree.adicionar(points[i]);
	}

	qtree.show(ctx);

	var rangeW = 200;
	var rangeH = 200;
	var rangeX = (400 - rangeW);
	var rangeY = (400 - rangeH);

	var range = new Rectangle(rangeX, rangeY, rangeW, rangeH);
	ctx.strokeStyle = "#00FF00";
	ctx.lineWidth = 2;
	ctx.strokeRect(range.x, range.y, range.w, range.h);

	//usar o quadtreee para consultar o que está dentro do bounding volume range
	var points = qtree.consultar(range);

	for (var i = 0; i < points.length; i++) {
		ctx.fillStyle = "#00FF00";
		ctx.fillRect(points[i].x, points[i].y, 3, 3);
	}
}

function exec_quad(samples, points) {
	var canvas = document.getElementById('canvas');
	canvas.width = 400;
	canvas.height = 400;

	var ctx = canvas.getContext('2d');

	var boundary = new Rectangle(0, 0, 400, 400);
	var qtree = new QuadTree(boundary, 1);

	var totalPoints = [];

	for (var i = 0; i < samples; i++) {
		//adiciona um ponto na tela
		totalPoints.push(points[i]);
	}

	qtree.show(ctx);

	var rangeW = 200;
	var rangeH = 200;
	var rangeX = (400 - rangeW);
	var rangeY = (400 - rangeH);

	var range = new Rectangle(rangeX, rangeY, rangeW, rangeH);
	ctx.strokeStyle = "#00FF00";
	ctx.lineWidth = 2;
	ctx.strokeRect(range.x, range.y, range.w, range.h);

	//função para fazer na força os pontos e pegar os pontos dentro do bounding volume range.
	 for(var i = 0; i < totalPoints.length; i++){
		if(range.contains(totalPoints[i])){
			ctx.fillStyle = "#00FF00";
			ctx.fillRect(totalPoints[i].x, totalPoints[i].y, 3, 3);
		}else{
			ctx.fillStyle = "#FFFFFF";
			ctx.fillRect(totalPoints[i].x, totalPoints[i].y, 1, 1);
		}
	}
}