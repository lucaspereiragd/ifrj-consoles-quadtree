var p = [];
for (var i = 0; i < 500; i++) {
    p[i] = new Point(Math.random() * 400, Math.random() * 400);
}
console.time("500-quad_tree");
exec_quad(500, p);
console.timeEnd("500-quad_tree");
console.time("500-seq");
exec_seq(500, p);
console.timeEnd("500-seq");

for (var i = 0; i < 1000; i++) {
    p[i] = new Point(Math.random() * canvas.width, Math.random() * canvas.height);
}
console.time("1000-quad_tree");
exec_quad(1000, p);
console.timeEnd("1000-quad_tree");
console.time("1000-seq");
exec_seq(1000, p);
console.timeEnd("1000-seq");

for (var i = 0; i < 10000; i++) {
    p[i] = new Point(Math.random() * canvas.width, Math.random() * canvas.height);
}
console.time("10000-quad_tree");
exec_quad(10000, p);
console.timeEnd("10000-quad_tree");
console.time("10000-seq");
exec_seq(10000, p);
console.timeEnd("10000-seq");

for (var i = 0; i < 100000; i++) {
    p[i] = new Point(Math.random() * canvas.width, Math.random() * canvas.height);
}
console.time("100000-quad_tree");
exec_quad(100000, p);
console.timeEnd("100000-quad_tree");
console.time("100000-seq");
exec_seq(100000, p);
console.timeEnd("100000-seq");

for (var i = 0; i < 1000000; i++) {
    p[i] = new Point(Math.random() * canvas.width, Math.random() * canvas.height);
}
console.time("1000000-quad_tree");
exec_quad(1000000, p);
console.timeEnd("1000000-quad_tree");
console.time("1000000-seq");
exec_seq(1000000, p);
console.timeEnd("1000000-seq");